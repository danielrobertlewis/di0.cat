import eventlet
eventlet.monkey_patch()

from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
import glob
import os



#
# Flask app construction notes:
#
# If neither of the static_ variables are set, it defaults to 'static' for both.
#     static_folder sets the folder on the filesystem
#     static_url_path sets the path used within the URL
#
# template_folder
#     setting this allows us to have index.html in the repo root
#
app = Flask(__name__, 
	        template_folder='../di0.cat/', 
	        static_folder='public',
            static_url_path='')

#
# SocketIO construction notes:
#
# These helped me when debugging client/server library version mismatch
#     engineio_logger=True
#     logger=True
#
# cors_allowed_origins="*"
#     set this when troubleshooting, never looked back
#
socketio = SocketIO(app, 
	                cors_allowed_origins="*", 
	                async_mode='eventlet', 
	                engineio_logger=True, 
	                logger=True)

#
# default route for http://di0.cat
#
@app.route('/')
def index():
    return render_template('index.html')
#
# default route that accepts POST'ing logs
#
@app.route('/', methods=['POST'])
def result():
    #print(request.form['foo']) # should display 'bar'
    #return 'Received POST !' # response to your request.

    #works
    #socketio.emit('after connect',  {'data':'post received'})

    #works
    #    curl -d "messageparam=something" -X POST localhost:5000
    #socketio.emit('after connect',  {'data':request.form['messageparam']})

    #works
    #    curl -d "data=LOG ME. I'M A LOG." -X POST di0.cat
    #socketio.emit('after connect',  {'data':request.form['data']})

    # NOTE:
    #
    #      I used a hack to shorten the curl call.
    #
    #      before:
    #            curl di0.cat -X post -d "data=somedata"
    #      after:
    #            curl di0.cat -X post -d "somedata"
    #
    #      Based upon the following 2 things being true:
    #         1) an http request has key-value request parameters.
    #         2) an http request param key can be >10million characters
    #
    #      We are using this knowledge to shorten the curl call a bit:
    #         3) for the purposes of di0.cat, the entire string passed, 
    #            is usually considered the key, no matter how many 
    #            characters and spaces it has...
    #         4) just in case someone passes actual key-value pairs the 
    #            right way, we send both values to the client page via 
    #            websockets.

    #works!!!!!!!!!!!
    #    curl di0.cat -X post -d "somedata and other text"
    for key, value in request.form.items():
        if key:
            #print(key)
            socketio.emit('after connect',  {'data':key})
        if value:
            #print(value)
            socketio.emit('after connect',  {'data':value})

    if request.content_type == "text/plain":
        socketio.emit('after connect', {'data': request.data.decode('utf8')})
    
    return ':: di0.cat :: POST received.'



#
# ls for http://di0.cat/<folder>
#
def getFileList(route):
    folder = (os.path.abspath(os.path.dirname(__file__)) + 
                 os.path.sep + 'public' + 
                 os.path.sep + route + 
                 os.path.sep + '*')

    folderContents = glob.glob(folder)
    #returnString = folder + ':<br />'
    returnString = ''

    for file in list(folderContents):
    	#print(file)
    	returnString += os.path.basename(file) + '\n'

    return returnString


#
# curl available routes (which match to real folders)
#
@app.route('/routes')
def ls_routes():
    return "conf\ncont\nlang\nprov\ntool\n"


#
# list static files available in each real folder
#
@app.route('/conf')
def ls_conf():
    return getFileList('conf')

@app.route('/cont')
def ls_cont():
    return getFileList('cont')

@app.route('/lang')
def ls_lang():
    return getFileList('lang')
    
@app.route('/prov')
def ls_prov():
    return getFileList('prov')

@app.route('/tool')
def ls_tool():
    return getFileList('tool')

#
# just a nice little message for the client when it connects
#
@socketio.on('connect')
def test_connect():
    socketio.emit('after connect',  {'data':'Connection established.'})

#
# run di0.cat server
#
if __name__ == '__main__':
    #socketio.run(app)
    # https://flask-socketio.readthedocs.io/en/latest/deployment.html#embedded-server
    print('starting di0.cat server')
    socketio.run(app, debug=True, host='0.0.0.0', port=80)

# SOURCE:
# -------
# https://pythonprogramminglanguage.com/python-flask-websocket/

# DEPENDENCIES:
# -------------
# FIX FOR 400 ERRORS from client and server
# caused by mismatch in client and server libraries
#    pip install --upgrade python-socketio==4.6.0
#    pip install --upgrade python-engineio==3.13.2
#    pip install --upgrade Flask==1.0.2
#    pip install --upgrade Flask-SocketIO==4.3.1
#    pip install eventlet
# NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOPE. FUCK THE ABOVE. use the latest versions of all of em.
# Tested good with latest on 2021.10.26 (works locally and on production system)
#     python-socketio 5.4.1
#     python-engineio 4.2.1
#     Flask 2.0.2
#     Flask-SocketIO 5.1.1
#     eventlet 0.32.0