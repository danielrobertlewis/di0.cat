
# di0.cat

## It's web.

- http://di0.cat to be exact.

## It's fast.

- STREAM: In-memory -- nothing touches the disk unless you add it to the repo.

- WRITE: Make your local project web hard in under 24 characters. (From any operating system, any version.... without installing -- anything.)

- READ: It's like a CDN, but without any of the speed or scalability or featureset of a CDN. It does do the static hosting part...

## It's small:

    - under 50 lines of code
    - under 50 lines of markup
    - under 50 lines of markdown

    ... but that's only on the server-side, so who cares anyway?

- I guess, to be fair, you do need some sort of http client and an internet connection as a local dependency. and electrical power.

## It's simple.

- List routes:

    `curl di0.cat/routes`

- List available static content inside a route:

    `curl di0.cat/lang`

- Dump a code snippet to stdout:

    `curl di0.cat/lang/cs`
 
- Dump a code snippet to file:

    `curl di0.cat/lang/cs >> NewClass.cs`

- Post log data to default route (shows on di0.cat landing page)

    `curl di0.cat -X post -d "stuff"`
 
- Post log data to custom route (shows at user-specified route)

	`curl di0.cat -X post -d "route_someproject=stuff"`

- Open browser to custom route

	`chrome http://di0.cat/someproject`

## It's fun.

- Use it to stream logs from your raspberry pi projects.

- Use it to host your own "gist" server, but better, because fuck github.

- Use it as an ad-hoc multiuser realtime chatroom? Sure why not.

- Hey, look... any prototype you're working on needs a logger, and you know damn well you want it to be web -- as fast as webbingly possible. This tool gives you the world in 24 characters, without any dependencies, and it runs on any OS released in the last 50 years. imean c'mon -- if you're not having fun yet, you're just a bad person inside.

Cheers.

di0